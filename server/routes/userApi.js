const express = require('express');
const router = express.Router();
const path = require("path");

var User = require("../models/user");

router.post('/signup', function(req, res){
    var user = User();
    user.userId = req.body.userId;
    user.name = req.body.name;
    user.phoneNo = req.body.phoneNo;
    user.gender = req.body.gender;
    user.height = req.body.height;
    user.weight = req.body.weight;
    user.age = req.body.age;
    user.pulseRate = req.body.pulseRate;
    user.temperature = req.body.temperature;
    user.bloodPressure = req.body.bloodPressure;


        user.save(function(err, savedUser){
            if(err){
                var message = 'User already registered!';
                if (err.errors) {
                }
                obj = {
                    success: false,
                    message: message,
                    status: 500
                }
                return res.status(500).send(obj);
            }else{
                    obj = {
                        success: true,
                        message: 'Registered successfully, Please check your email to activate your account!',
                        status: 200
                    }
                    return res.status(200).send(obj);
            }
        })
    
});

router.get('/getAllPatients', function(req, res){
    
    User.find(function(err, patients){
        if(err){
            console.log(err);
            obj = {
                success: false,
                message: 'User api failed', 
                status: 500
            }
            return res.status(500).send(obj);
        }else{
            obj = {
                success: true,
                message: 'Getting user list succesfully!',
                data: patients,
                status: 200
            }
            return res.status(200).send(obj);
        }
    });
});

module.exports = router;