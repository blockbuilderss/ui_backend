var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dailyReportSchema = new Schema({
    userId:String,
    pulseRate:String,
    reportDate: Date,
    bloodPressure:String,
    temperature:String,
    medicine1:String,
    medicine2:String,
    weight:String
});

module.exports = mongoose.model('dailyReport', dailyReportSchema);