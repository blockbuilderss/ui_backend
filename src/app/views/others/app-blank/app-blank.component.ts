import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Jsonp } from '@angular/http';
import 'rxjs/add/operator/toPromise'; 

@Component({
  selector: 'app-blank',
  templateUrl: './app-blank.component.html',
  styleUrls: ['./app-blank.component.css']
})
export class AppBlankComponent implements OnInit {
  selectedId:String;
  userDetails;
  dailyReportsArray = [];
  usersArray = [];
  
  user = {
    userId: '',
    name: '',  
    phoneNo: '',
    gender: '',
    age: '',
    height: '',
    weight: '',
    pulseRate: '',
    temperature: '',
    bloodPressure: '',
    bloodSugarLevel: ''
  };

  dailyReport = {
    reportDate: new Date(),
    pulseRate:'',
    bloodPressure:'',
    temperature:'',
    medicine1:'',
    medicine2:'',
    weight:''
  };
  
  showThankYouMsg:boolean = false;
  showAddReportForm:boolean=false;

  constructor(private http: Http){}

  ngOnInit(){
    this.getAllUsers();
  }

  addDailyReport(userId){
    console.log(userId);
    var value = {'patientId': userId}
    var url = '/patient/getPatientById';
    let headers = new Headers({ 'Content-Type' : 'application/json' });
    let options = new RequestOptions({ headers : headers });
    console.log(value);
    return this.http.post(url, JSON.stringify(value), options)
    .toPromise()
    .then((res)=>{
      this.showAddReportForm=false;
      console.log(res);
      res = JSON.parse(res['_body']);
      this.showAddReportForm=true;
      this.dailyReport = {
        reportDate: new Date(),
        pulseRate:res['data'].pulseRate,
        bloodPressure:'',
        temperature:res['data'].temperature,
        medicine1:'',
        medicine2:'',
        weight:''
      }
    })
    .catch((err)=>{
      console.log(err);
    })
   

  }

  onSubmitDailyReport(value){
    value['userId'] = this.selectedId['value'];
    var url = '/dailyReport/updateDailyReport';
    let headers = new Headers({ 'Content-Type' : 'application/json' });
    let options = new RequestOptions({ headers : headers });
    console.log(value);
    return this.http.post(url, JSON.stringify(value), options)
    .toPromise()
    .then((res)=>{
      this.showAddReportForm=false;
      console.log(res);
      res = JSON.parse(res['_body']);
      this.dailyReportsArray.push(res['data']);
    })
    .catch((err)=>{
      console.log(err);
    })
  }

  getAllUsers(){
    var url = '/user/getAllPatients';
    return this.http.get(url)
    .toPromise()
    .then((res:Response)=>{
      res = JSON.parse(res['_body']);
      this.usersArray = res['data'];
    })
    .catch((err)=>{
      console.log(err);
    })
  }

  getDailyReportById(){
    var url = '/dailyReport/getDailyReportsById';
    let headers = new Headers({ 'Content-Type' : 'application/json' });
    let options = new RequestOptions({ headers : headers });
    var value = {
      userId: this.selectedId['value']
    }
    return this.http.post(url, JSON.stringify(value), options)
    .toPromise()
    .then((res)=>{
      console.log(res);
      res = JSON.parse(res['_body']);
      this.dailyReportsArray = res['data'];
    })
    .catch((err)=>{
      console.log(err);
    })
  }

  selectedPatient(value){
    this.selectedId = value;
    this.usersArray.forEach(element => {
      if(element._id==value.value){
        this.userDetails = element;
        this.getDailyReportById();
      }
    });
  }

  onKey(event){
    if(Number(this.user.phoneNo)==60){
      console.log("Pressure");
    }
  }

  goToHome(){
    this.showThankYouMsg = false;
    this.user = {
      userId: '',
      name: '',  
      phoneNo: '',
      gender: '',
      age: '',
      height: '',
      weight: '',
      pulseRate: '',
      temperature: '',
      bloodPressure: '',
      bloodSugarLevel: ''
    };  
  }

  onSubmitUser(value){
    console.log(value);
    this.submitUser(value);
  }

  submitUser(userobj){
    var url = '/user/signup';
    let headers = new Headers({ 'Content-Type' : 'application/json' });
    let options = new RequestOptions({ headers : headers });
    return this.http.post(url, JSON.stringify(userobj), options)
    .toPromise()
    .then((res)=>{
      this.showThankYouMsg=true;
      console.log(res);
      this.getAllUsers();
    })
    .catch((err)=>{
      console.log(err);
    })
  }
}

